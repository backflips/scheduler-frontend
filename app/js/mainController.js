angular.module('mainCtrl', [])


	.controller('mainController', function($rootScope, $scope, $location, $state) {
		var vm = this
		
		$rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
			var name = toState.name

			
			switch(name){
				case 'home':
				case 'about':
				case 'login.form':
				case 'login.register':
				case null:
					console.log("Continuing")
					break
				default:
					if(!$scope.loggedIn) {
						e.preventDefault()
						$scope.loginFrom = toState.name
						console.log("Redirecting to login")
						$state.go('login.form')
					}
					
					
			}
				
		})
		
		$scope.loggedIn = false
		
		$scope.Login = function() {
			$scope.loggedIn = true
			$state.go($scope.loginFrom)
		}
		
		$scope.Logout = function() {
			$scope.navCollapsed = true
			$scope.loggedIn = false
			$state.go('home')
		}
	})
	
	
