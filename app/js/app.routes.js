var routes = angular.module('app.routes', ['ui.router', 'testCtrl', 'test2Ctrl'])

routes.config(function router($stateProvider, $urlRouterProvider, $locationProvider) {
	$urlRouterProvider.otherwise('/')
	
	$locationProvider.html5Mode(true)
	
	$stateProvider
	
		/*.state('home'), {
			url: '/home' ,
			templateUrl: 'partials/navbar.html'/*,
			controller: function navCont($state) {
					$state.transitionTo('app.home')
			}
		}*/

		
		.state('home', {
			url: '/',
			templateUrl: 'partials/partial-home.html'	
		})
		
		.state('about', {
			url: '/about',
			templateUrl: 'partials/about.html'
		})
		
		.state('login', {
			templateUrl: 'partials/login/login.html'
		})
		
		.state('login.form', {
			url:'/login',
			templateUrl: 'partials/login/login.form.html',
			controller: 'test2Controller',
			controllerAs: 'asdf'
		})
		
		.state('login.register', {
			url: '/register',
			templateUrl: 'partials/login/login.register.html'
		})
		
		.state('schedule', {
			templateUrl: 'partials/schedule/schedule.html'
		})
		
		.state('schedule.calendar', {
			url: '/schedule',
			templateUrl: 'partials/schedule/schedule.calendar.html',
			controller: 'testController',
			controllerAs: 'test'
		})
		
		.state('schedule.list', {
			url:'/schedule/list',
			templateUrl: 'partials/schedule/schedule.list.html'
		})
})

var test2Ctrl = angular.module('test2Ctrl', [])

test2Ctrl.controller('test2Controller', ['$scope', function($scope) {
	var vm = this
	
	
	vm.Login = function() {
		$scope.Login()
	}
}])

var testCtrl = angular.module('testCtrl', ['ui.calendar'])

testCtrl.controller('testController', ['$scope', 'uiCalendarConfig', function($scope, uiCalendarConfig) {
	var self = this
	
	$scope.newTest = {}
	
	var today = new Date()
    var tomorrow = new Date()
    tomorrow.setHours(20)
    tomorrow.setDate(31)
	
	$scope.events = [
            {
            title: 'test',
            start: '2016-01-13T08:00:00',
            allDay: false
            },
            {
				id:	123,
                title: 'test3',
                start: tomorrow,
                end: tomorrow,
				description: "This is a test.  A really cool test.  One of the best around",
				color: 'green',
				textColor: 'orange'
            },
            {
                title: 'test2',
                start: tomorrow,
                end: tomorrow,
				color: '#378006'
            }
        ]
	
	$scope.eventSources = [{
        events: $scope.events
    }]
	
	$scope.events = $scope.eventSources[0].events 
	
	self.edit = function() {
		self.editDetails = true
	}
	
	self.testtest = function() {
		self.event.title = "IT WORKS"
		self.title= self.event.title
		var test = uiCalendarConfig.calendars.myCalendar.fullCalendar('clientEvents', self.event._id)[0]
		uiCalendarConfig.calendars.myCalendar.fullCalendar('updateEvent', test)
		
		console.log($scope.eventSources)
		
	}
	
	self.updateEvent = function() {
		self.editDetails = false;
		var test = uiCalendarConfig.calendars.myCalendar.fullCalendar('clientEvents', self.event._id)[0]
		uiCalendarConfig.calendars.myCalendar.fullCalendar('updateEvent', test)
	}
	
	$scope.eventClick = function(event, jsEvent, view) {
        //$scope.title = event.title
		
		self.event = event
		
		self.showDetails= true;
		
		//console.log(self.event)
		
		$("#details")[0].scrollIntoView()
		
		if(view.name != 'agendaDay') {
			uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate', event._start)
			uiCalendarConfig.calendars.myCalendar.fullCalendar('changeView', 'agendaDay')
		}
    }
	
	$scope.dayClick = function(date, allDay, jsEvent, view ) {
		console.log(self.test)
	}
	
	self.showEvents = function() {
		console.log(uiCalendarConfig.calendars.myCalendar.fullCalendar('clientEvents'))
	}
	
	self.showEvent = function() {
		$scope.eventSources[0].events.push(
			{
            title: 'test',
            start: '2016-01-16T08:00:00',
            allDay: false
            }
		)
		
	}
	
	self.moveEvent = function(event, delta, revertFunc, jsEvent, ui, view) {
		self.event = event
		uiCalendarConfig.calendars.myCalendar.fullCalendar('updateEvent', event)
	}
	
	self.resizeEvent = function(event, delta, revertFunc, jsEvent, ui, view) {
		self.event = event
		uiCalendarConfig.calendars.myCalendar.fullCalendar('updateEvent', event)
	}
	
	$scope.uiConfig = {
      calendar:{
        minTime: "06:00:00",
		maxTime: "22:00:00",
		height: 800,
        editable: true,
        header:{
          left: 'month agendaWeek agendaDay',
          center: 'title',
          right: 'today prev,next'
        },
        dayClick: $scope.dayClick,
        eventClick: $scope.eventClick,
		eventDrop: self.moveEvent,
		eventResize: self.resizeEvent
      }
    }
	
	self.showDetails = false;
	self.editDetails= false;
}])